#!/usr/bin/python3
#
# Given a year and a month, find all the tournaments and extract the
# Archon info from all of them that have it

import csv
import datetime
import pathlib
import sys
import requests
from io import StringIO

import yaml

sys.path.insert(0, '.')
from parse_event_file import do_parse_file

CACHE_DIR = 'cached'


def parse_event(url, aData, year):
    filename = url.split('/')[-1]
    cached = pathlib.Path('cached/' + filename).absolute()
    if cached.exists():
        with cached.open('r') as f:
            dData = yaml.safe_load(f)
        bOK = dData['valid']
        if bOK:
            oDate = dData['date']
            sName = dData['name']
            sType = dData['type']
            iRounds = dData['rounds']
            fVPs = float(dData['vps'])
            iSeed = dData['seed']
            iPlayers = dData['players']
    else:
        sub_r = requests.get(url)
        buf = StringIO(sub_r.text)
        buf.seek(0)
        bArchon, bOK, tData = do_parse_file(buf, False)
        if bArchon:
            dData = {}
            dData['valid'] = bOK
            sName, oDate, sType, iRounds, iPlayers, fVPs, iSeed = tData
            dData['date'] = oDate
            dData['name'] = sName
            dData['type'] = sType
            if bOK:
                dData['rounds'] = iRounds
                dData['vps'] = '%.1f' % fVPs
                dData['seed'] = iSeed
                dData['players'] = iPlayers
            with cached.open('w') as f:
                yaml.dump(dData, f)
    if bOK:
        if oDate.year == int(year):
            CSV_Data = (sName, f'{oDate:%Y-%m-%d}', sType, iRounds,
                        iPlayers, fVPs, iSeed)
            aData.append(CSV_Data)


def get_events(year, month):
    aSubUrls = []
    url = f'http://www.vekn.net/event-calendar?date={year}-{month}'

    r = requests.get(url)
    if r.status_code != 200:
        print(f"Failed to retrieve {url}. Status code {r.status_code}")
        sys.exit(0)
    data = r.content
    for line in data.splitlines():
        if b'/event/' in line:
            start_pos = line.index(b'/event-calendar/event/')
            section = line[start_pos:start_pos + 30]
            snippet = section.split(b'"')[0].decode('ascii')
            sub_url = 'http://www.vekn.net' + snippet
            if sub_url in aSubUrls:
                continue
            aSubUrls.append(sub_url)
    return aSubUrls


if __name__ == "__main__":
    aData = []

    if len(sys.argv) == 3:
        year = sys.argv[1]
        month = sys.argv[2]

        aSubUrls = get_events(year, month)
    else:
        year = sys.argv[1]
        aSubUrls = []
        for month in range(1, 13):
            aSubUrls.extend(get_events(year, month))
    for sub_url in aSubUrls:
        parse_event(sub_url, aData, year)

    output = StringIO()
    writer = csv.writer(output)

    writer.writerow(("Name", "Date", "Type", "Rounds",
                     "Number of Players", "Winner's VPs", "Winner's Seeding"))
    for row in aData:
        writer.writerow(row)

    output.seek(0)
    print(output.read())


