#!/usr/bin/python3
#
# Given a year and a month, find all the tournaments and extract the
# Archon info from all of them that have it

import datetime
import os
import pathlib

import yaml

CACHE_DIR = 'cached'

tNormal = ('National Championship', 'National Qualifier', 'Continental Championship',
           'Continental Qualifier', 'Standard Constructed', 'Mini-Qualifier')

def parse_event(path):
    cached = path.absolute()
    if cached.exists():
        with cached.open('r') as f:
            dData = yaml.safe_load(f)
        if dData['date'] == 'Unknown':
            return (False, None, None, None)
        bOK = dData['valid']
        oDate = dData['date']
        sName = dData['name']
        sType = dData['type']
        return (bOK, oDate, sName, sType)
    else:
        raise RuntimeError(f"Unknown file {path}")


def main():
    dInfo = {}
    for filename in os.listdir(pathlib.Path(CACHE_DIR)):
        tData = parse_event(pathlib.Path(CACHE_DIR + '/' + filename))
        if tData[1] != 'Unparsable Date':
            oDate = tData[1]
            dInfo.setdefault(oDate.year, [])
            dInfo[oDate.year].append(tData)
    for year in sorted(dInfo):
        aNormal = [0, 0, 0]
        dTypes = {}
        for tData in dInfo[year]:
            bOK, _, _, sType = tData
            dTypes.setdefault(sType, [0, 0, 0])
            if sType in tNormal:
                aNormal[0] += 1
                if bOK:
                    aNormal[1] += 1
                else:
                    aNormal[2] += 1
            dTypes[sType][0] += 1
            if bOK:
                dTypes[sType][1] += 1
            else:
                dTypes[sType][2] += 1
        print(year)
        for sType in sorted(dTypes):
            print(f'   {sType}')
            iTot, iArchon, iNoArchon = dTypes[sType]
            print(f'      Total events: {iTot}')
            print(f'          Archon data  : {iArchon}')
            print(f'          No Archon    : {iNoArchon}')
        print(f' == All standard tournaments ==')
        iTot, iArchon, iNoArchon = aNormal
        print(f'   Total events: {iTot}')
        print(f'          Archon data  : {iArchon}')
        print(f'          No Archon    : {iNoArchon}')


if __name__ == "__main__":
    main()
    

