#!/usr/bin/python3
#
# Given a downloaded python event, either
# Report that we cannot find the archon info, or
# extract the tournament type, the number of rounds,
# the Winners name, the VPs in the final
# and the seeding of the player


import datetime
import os
import sys

from html.parser import HTMLParser


class State:
    # A state in the HTML parser
    def __init__(self, oOther=None):
        self._dData = {}
        if oOther:
            self._dData = oOther.get_data()

    def transition(self, sTag, dAttr):
        """Transition from one state to another"""
        raise NotImplementedError

    def data(self, sData):
        pass

    def get_data(self):
        # Query the data dictionary
        return self._dData


class StartState(State):
    """Default state"""

    def transition(self, sTag, dAttr):
        if sTag == 'div' and dAttr.get('class') == 'componentheading':
            return InName(self)
        if sTag == 'table' and dAttr.get('class') == "contentpaneopen vekn":
            return InTable(self)
        return self


class InName(State):

    def transition(self, sTag, dAttr):
        if sTag == '/h3' or sTag == '/div':
            return StartState(self)
        return self

    def data(self, sData):
        self._dData['Tournament Name'] = sData


class InTable(State):
    """Content table"""

    def transition(self, sTag, dAttr):
        if sTag == 'b' and 'Date' not in self._dData:
            return DateInfo(self)
        elif sTag == 'br' and 'Type' not in self._dData:
            return TourneyType(self)
        if sTag == 'th' and dAttr.get('align') == 'left' and 'Rounds' not in self._dData:
            return RoundInfo(self)
        elif sTag == 'th' and dAttr.get('align') is None and 'Finals' not in self._dData:
            return FinalInfo(self)
        elif sTag == '/table':
            return StartState(self)
        return self

class DateInfo(State):
    """Find the tournement type"""
    def transition(self, sTag, dAttr):
        if sTag == '/b':
            return InTable(self)
        return self

    def data(self, sData):
        """Extract date"""
        if 'Date' not in self._dData:
            sDate = sData.split(',')[0]
            oDate = datetime.datetime.strptime(sDate, "%d %B %Y").date()
            self._dData['Date'] = oDate


class TourneyType(State):
    """Find the tournement type"""
    def transition(self, sTag, dAttr):
        if sTag == '/p':
            return InTable(self)
        return self

    def data(self, sData):
        """Extract data"""
        if 'Type' not in self._dData:
            self._dData['Type'] = sData


class RoundInfo(State):
    """Find the number of rounds (may be missing)"""
    def transition(self, sTag, dAttr):
        if sTag == '/tr':
            return InTable(self)
        return self

    def data(self, sData):
        """Extract data"""
        if sData == 'Rounds':
            self._dData['Rounds'] = ''
        elif 'Rounds' in self._dData and not self._dData['Rounds']:
            self._dData['Rounds'] = sData


class FinalInfo(State):
    """Find the finalists and there scores"""
    def transition(self, sTag, dAttr):
        if sTag == '/tbody':
            return InTable(self)
        if sTag == 'tr':
            self._dData.setdefault('Players', 0)
            if 'Finalists' not in self._dData:
                self._dData['Finalists'] = []
            self._dData['Players'] += 1
            if len(self._dData['Finalists']) < 5:
                return PotentialFinalist(self)
        return self

class PotentialFinalist(State):

    def transition(self, sTag, dAttr):
        if sTag == '/tr':
            return FinalInfo(self)
        if sTag == 'td':
            return Position(self)
        return self

class Position(State):
    
    def transition(self, sTag, dAttr):
        if sTag == '/td':
            return Name(self)
        return self

class Name(State):
    def transition(self, sTag, dAttr):
        if sTag == '/td':
            return PrelimGW(self)
        return self

    def data(self, sData):
        """Extract data"""
        aFinalist = [sData]
        self._dData['Finalists'].append(aFinalist)

class PrelimGW(State):
    def transition(self, sTag, dAttr):
        if sTag == '/td':
            return PrelimVP(self)
        return self

    def data(self, sData):
        """Extract data"""
        aFinalist = self._dData['Finalists'][-1]
        aFinalist.append(int(sData))


class PrelimVP(State):
    def transition(self, sTag, dAttr):
        if sTag == '/td':
            return PrelimTP(self)
        return self

    def data(self, sData):
        """Extract data"""
        aFinalist = self._dData['Finalists'][-1]
        aFinalist.append(float(sData))


class PrelimTP(State):
    def transition(self, sTag, dAttr):
        if sTag == '/td':
            return FinalVP(self)
        return self

    def data(self, sData):
        """Extract data"""
        aFinalist = self._dData['Finalists'][-1]
        aFinalist.append(int(sData))


class FinalVP(State):
    def transition(self, sTag, dAttr):
        if sTag == '/td':
            return FinalInfo(self)
        return self

    def data(self, sData):
        """Extract data"""
        aFinalist = self._dData['Finalists'][-1]
        aFinalist.append(float(sData))



def parse_archon_info(aFinalists):
    # Winner is the first entry
    sWinner = aFinalists[0][0]
    fVPs = aFinalists[0][4]

    # Order players by GWs, VPs then TPs to determine seeding
    maxGW = 0
    maxVPs = 0
    iSeed = 0
    aSeeds = []
    for aInfo in aFinalists:
        # 10000 * GW + 1000*VPs + TPs, so we can sort on this easily
        iScore = aInfo[1] * 10000 + 1000*aInfo[2] + aInfo[3]
        aSeeds.append((iScore, aInfo[0]))
    aSeeds.sort(key=lambda x: -x[0])  # sort on score, descending
    aNames=[x[1] for x in aSeeds]
    iSeed = aNames.index(sWinner)
    return sWinner, fVPs, iSeed + 1


class EventParser(HTMLParser):

    def __init__(self):
        super().__init__()
        self._oState = None

    def reset(self):
        super().reset()
        self._oState = StartState()

    def handle_starttag(self, sTag, aAttr):
        self._oState = self._oState.transition(sTag.lower(), dict(aAttr))

    def handle_endtag(self, sTag):
        self._oState = self._oState.transition('/' + sTag.lower(), {})

    def handle_data(self, sData):
        sData = sData.strip()
        if sData:
            self._oState.data(sData)

    def handle_charref(self, sName):
        pass

    def handle_entityref(self, sName):
        pass

    def parse(self, fIn):
        self.reset()
        for sLine in fIn:
            self.feed(sLine)
        return self._oState.get_data()


def do_parse_file(fIn, bVerbose=False):
    parser = EventParser()
    dInfo = parser.parse(fIn)
    bLegal = True
    bArchon = True
    if 'Type' not in dInfo:
        if bVerbose:
            print("Failed to extract tournament type")
        bLegal = False
        dInfo['Type'] = 'Unknown'
    elif 'limited' in dInfo['Type'].lower():
        if bVerbose:
            print("Skipping draft tournamnet")
        bLegal = False
    elif 'unsanctioned' in dInfo['Type'].lower():
        if bVerbose:
            print("Skipping unsanctioned tournamnet")
        bLegal = False
    if 'Tournament Name' not in dInfo:
        bLegal = False
        dInfo['Tournament Name'] = "Unparsable Tournament Name"
    if 'Date' not in dInfo:
        bLegal = False
        dInfo['Date'] = "Unparsable Date"
    if bLegal:
        if 'Rounds' not in dInfo:
            if bVerbose:
                print('Failed to extract round info')
            bLegal = False
        elif '+ final' not in dInfo['Rounds'].lower():
            if bVerbose:
                print("Skipping tournament with no listed final")
            bLegal = False
        elif dInfo['Rounds'].lower() not in ('3 + final', '2 + final'):
            if bVerbose:
                print(f'Unknown round count {dInfo["Rounds"]}')
            bLegal = False
    if bLegal:
        if 'Finalists' not in dInfo:
            if bVerbose:
                print("Skipping tournament with no Archon")
            bLegal = False
            bArchon = False
        else:
            fTotVPs = 0
            for aFinal in dInfo['Finalists']:
                fTotVPs += aFinal[4]
            if fTotVPs < 2.5:
                if bVerbose:
                    print(f"Incorrect VP total: {fTotVPs}")
                bLegal = False
    if bLegal:
        sWinner, fVPs, iSeed = parse_archon_info(dInfo['Finalists'])
        if bVerbose:
            print(dInfo['Tournament Name'])
            print(dInfo['Date'])
            print(dInfo['Type'])
            print(dInfo['Rounds'])
            print(dInfo['Players'])
            print(sWinner, fVPs, iSeed)
        if dInfo['Rounds'].lower() == '3 + final':
            iRounds = 3
        else:
            iRounds = 2
        return True, True, (dInfo['Tournament Name'],
                            dInfo['Date'],
                            dInfo['Type'],
                            iRounds,
                            dInfo['Players'],
                            fVPs,
                            iSeed)
    return bArchon, False, (dInfo['Tournament Name'],
                            dInfo['Date'],
                            dInfo['Type'], -1, -1, 0.0, -1)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Must provide a filename")
        sys.exit(1)
    if not os.path.exists(sys.argv[1]):
        print(f"Missing file {sys.argv[1]}")
        sys.exit(1)
    with open(sys.argv[1], 'r') as f:
        do_parse_file(f, True)
